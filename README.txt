Callouts module will allow for displaying multiple 'callout' nodes within a region.
These 'callout' nodes (a new content type that is created) are displayed based on
the Taxonomy terms set for the 'parent node' (the page the user is on).  This
way you can display nodes that are related to the parent node. (there is also an option
to display random nodes).  This strategy can be useful for many different aspects such as
displaying related products in an e-commerce enviroment, announcments for specific site sections,
ect... When a page is loaded, Callouts retrieves its node id (nid), followed by the term id (tid)
for each of its taxonomy terms.  Callouts then retrieves a list of callout node ids (nids) that
have the same taxonomy terms (or if no callout notes are found to be related, and random is enabled
retrieves random callout nodes).   Callouts then displays these nodes (title and body) into whichever
block the module has been placed in.  Admin's have the abilities to choose the MAX number of callout
nodes to display, and to enable/disable if random nodes will be displayed.

HOW TO USE IT:
1. Install the module
2. Go to node/add and create a new 'Callout' node (be sure to set taxonomy terms)
3. Go to admin/build/block and place the 'Callouts' block into the region of your choice
4. Go to an existing node with similar taxonomy terms to see your callouts

HOW TO MANAGE
1. Go to admin/content/callouts/settings
2. Set your settings (or leave as default)

