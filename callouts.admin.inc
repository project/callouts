<?php

/**
 * @file
 * Admin page callbacks for the callouts module
 */

/**
 * Returns an administrative overview of all callouts.
 */
function callouts_admin_overview() {
  $rows = array();
  foreach (callouts_get_callouts() as $nid=>$title) {
    $rows[] = array(l($title,'node/'.$nid), l(t('edit'), "node/".$nid."/edit"));
  }
  $headers = array(t('Callouts'), t('Operations'));

  return theme('table', $headers, $rows);
}

/**
 * Builds and returns the callouts settings form.
 *
 * @see callouts_admin_settings_validate()
 *
 * @ingroup forms
 */
function callouts_admin_settings() {
  $form['callouts_max_display'] = array(
    '#type' => 'textfield',
    '#title' => t('Max number of Callout blocks'),
	'#size'=>10,
	'#maxlength'=>20,
    '#default_value' => variable_get('callouts_max_display', 0),
    '#description' => t('Enter the max number of blocks you want to display, enter 0 for unlimited'),
    '#required' => TRUE,
  );
  $form['callouts_show_random'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Random if page has no Taxonomy'),
    '#default_value' => variable_get('callouts_show_random', 1),
    '#description' => t('If a page does not have any Taxonomy, or no callouts with Taxonomy is found, random callout blocks will be displayed'),
  );
  $form['array_filter'] = array('#type' => 'value', '#value' => TRUE);
  $form['#validate'][] = 'callouts_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * Validate the book settings form.
 *
 * @see callouts_admin_settings()
 */
function callouts_admin_settings_validate($form, &$form_state) {
	$max = $form_state['values']['callouts_max_display'];
	if(!is_numeric($max)) form_set_Error('callouts_max_display',t($max.'Must provide a integer for Max Blocks'));
}

