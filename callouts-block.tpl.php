<?php

/**
 * @file callouts-block.tpl.php
 * Default theme implementation for displaying a callout block
 *
 * Available variables:
 * - $node: The callout node and all its data
 * - $delta: This is a numeric id connected to each module.
 *
 */
?>

<div id="callout-<?PHP echo $delta; ?>-<?PHP echo $node->nid; ?>">
	<h2><?php echo $node->title; ?></h2>
	<p><?PHP echo $node->body; ?></b>
</div>