<?PHP
/**
* Valid permissions for this module
* @return array An array of valid permissions for the callouts module
*/
function callouts_perm() {
  return array('create callout block','edit callout block','delete callout block');
} // function onthisdate_perm()

/**
 * Implements hook_access()
 */
 function callouts_access($op, $node, $account){
	return user_access($op.' callout block', $account);
 }

 	/**
	 * Implements hook_node_info()
	 */
	 function callouts_node_info(){
		return array(
			'Callout'=>array(
				'name'=>t('Callout'),
				'module'=>'callouts',
				'description'=>t('A callout block that is displayed based on Taxonomy terms'),
				'has_title'=>TRUE,
				'title_label'=>t('Block Title'),
				'has_body'=>TRUE,
				'body_label'=>t('Block Content'),
			)
		);
	 }
/**
* Wrapper around node_access() with additional checks for outline permissions.
* @see node_access()
*/
function callouts_node_access($op, $node, $account = NULL) {

  // load the node if it's not done already 
  if (!isset($node->type)) {
    $node = node_load(arg(1));
  }
  
  // if the node is not a YOURTYPE type, don't display the outline tab
  return ($node->type == 'callout' ? TRUE : FALSE);
}

/**
 * Menu item access callback - determine if the outline tab is accessible.
 */
function _callouts_outline_access($node) {
  return user_access('administer book outlines') && node_access('view', $node);
}
/** 
 * Implementation of hook_form()
 */
function callouts_form(&$node){
	$type = node_get_types('type', $node);
	//Existing Files
	if($type->has_title) {
		$form['title'] = array(
			'#type'=>'textfield',
			'#title'=>check_plain($type->title_label),
			'#required'=>TRUE,
			'#default_value'=>$node->title,
			'#weight'=>-5,
		);
	}
	
	if($type->has_body){
		$form['body_field'] = node_body_field(
			$node,$type->body_label,
			$type->min_word_count
		);
	}
	
	return $form;
}



/**
 * Implementation of hook_insert();
 */
 function callouts_insert($node){
	db_query('INSERT INTO {callout} (vid, nid) '."VALUES (%d, %d)", $node->vid, $node->nid);
 }

 /**
  * Implementation of hook_theme();
  */
 function callouts_theme() {
	return array(
		'callouts_block'=>array(
			'arguments' => array('node' => array(),'delta'=> NULL),
			'template' => 'callouts-block',
		),
	);
}
 

/**
* Implementation of hook_block
*/
function callouts_block($op = 'list', $delta = 0, $edit = array()) { 
  // list the blocks 
  if ($op == "list") {
     $block[0]["info"] = t('Callouts');
     return $block;
  } else if($op=='view') {
		//Get Module varaibles
		$max = variable_get('callouts_max_display', 0);
		if($max==0){
			$max=false;
		}
		$random = variable_get('callouts_show_random', 1) ;
		$random = ($random==1 ? true:false);
  
		$output='<div id="callouts">';
		//Check if we can find parent node id (the nid of the page)
		if($nid = callouts_get_parent_node()){
			//Get terms for parent node
			$terms = array_keys(callouts_get_taxonomy_terms($nid));
		}else $terms = false;
		
		if($terms){
			$callouts = callouts_get_callouts_by_terms($terms, $max);
		}else{
			$callouts = false;
		}
		
		//If we dont have any callouts, and random is enabled retrieve upto MAX callouts
		if($callouts===false && $random){
			$callouts = callouts_get_callouts_by_terms(FALSE,$max);
		}

		//If callouts, format theme for display
		if(is_array($callouts)){
			
			foreach($callouts as $c){
				$node = node_load($c);
				$output .= theme('callouts_block', $node, $delta, TRUE);
			}
		}
		
		$output.='</div>';
		return array('content'=>$output);
	}
} // end callouts_block

//Retrieve nid for page user is on (e.g: '[domain]/node/nid')
function callouts_get_parent_node(){
	if (arg(0) == 'node' && is_numeric(arg(1)) && is_null(arg(2))){
		return (int)arg(1);
	}
}

// Retrieves taxonomy terms for a given node.
// Returns an associative array formated array('tid'=>name)
function callouts_get_taxonomy_terms($nid){
	$terms = taxonomy_node_get_terms(node_load($nid));
	$temp = array();
	foreach($terms as $t){
		$temp[$t->tid] = $t->name;
	}
	return $temp;
}
/**
 * callouts_get_callouts(). Retrieves a list of callout node ID's and titles in an associative array
 * format is array('nid'=>'title');
 */
function callouts_get_callouts(){
	$callouts = db_query(db_rewrite_sql('SELECT a.nid,b.title FROM {callout} AS a, {node} AS b WHERE b.nid = a.nid'));
	$temp = array();
	while($c = db_fetch_array($callouts)){
		$temp[$c['nid']] = $c['title'];
	}
	$callouts = $temp;
	return $temp;
}

/**
 * callouts_get_callouts_by_terms().
 * @params	$terms	array	an array of Taxonomy term ids to check
 * @params	$max	int		the max number of Callout nodes to return
 */
function callouts_get_callouts_by_terms($terms=false, $max=false){
	$blocks = 'SELECT nid FROM {callout}';
	$blocks = db_query(db_rewrite_sql($blocks));
	
	$temp = array();
	//If checking taxonomy terms
	if($terms){
		//for each callback node
		while($b = db_fetch_array($blocks)){
			$b = $b['nid'];
			//get taxonomy terms for this node
			$taxonomy = callouts_get_taxonomy_terms($b);
			//for each taxonomy term
			foreach($taxonomy as $tid=>$name){
				//if we havent met our limit
				if($max && sizeof($temp)<$max){
					//if node term is in the terms we're checking, and node id is not already in array, add it
					if(in_array($tid, $terms) && !in_array($b, $temp)){
						$temp[] = $b;
						//continue to next node, no reason to keep checking
						continue;
					}
				}
			}
		}
	//Else no terms to check, 	
	}else{
		//add each node id to temp
		while($b = db_fetch_array($blocks)){
			$temp[] = $b['nid'];
		}
		//If have a max, and array is greater then max
		if($max && sizeof($temp)<$max){
			//randomize ids
			shuffle($temp);
			//shorten array to max size
			$temp = array_splice($temp, $max);
		}
		
	}
	$blocks = $temp;
	//if no blocks found return false
	return sizeof($blocks) > 0 ? $blocks : false;
}

/**
 * Implementation of hook_menu().
 */
 
function callouts_menu() {
  $items['admin/content/callouts'] = array(
    'title' => 'Callouts',
    'description' => "Manage your site's callouts outlines.",
    'page callback' => 'callouts_admin_overview',
    'access arguments' => array('access administration pages'),
    'file' => 'callouts.admin.inc',
  );
  $items['admin/content/callouts/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/content/callouts/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('callouts_admin_settings'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 8,
    'file' => 'callouts.admin.inc',
  );

  return $items;
}

/**
* Display help and module information
* @param path which path of the site we're displaying help
* @param arg array that holds the current path as would be returned from arg() function
* @return help text for the path
*/
function callouts_help($path, $arg) {
  $output = '';  //declare your output variable
  switch ($path) {
    case "admin/help#callouts":
      $output = '<p>'.  t("This Module create content nodes that are displayed in a block on a content page with related taxonomy terms.") .'</p>';
      break;
  }
  return $output;
} // function onthisdate_help



